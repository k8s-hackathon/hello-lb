# hello-k8s-lb

A simple k8s cluster deployment configured with external load-balancing.

```
helm repo add traefik https://containous.github.io/traefik-helm-chart
```

```
helm repo update
```

```
helm install -n traefik-lb traefik-lb -f k8s/traefik/values.yml traefik/traefik
```

```
kubectl apply -f k8s
```

Traefik helm config upgraden: 

```
helm upgrade -n traefik-lb traefik-lb -f k8s/traefik/values.yml traefik/traefik
```
Port forwarding:

```
 k port-forward -n hello-k8s-first service/hello-k8s-first 8080:80
```
